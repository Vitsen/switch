'use strict';

var gulp = require('gulp'),
    autoprefixer = require('gulp-autoprefixer'),
    //bourbon = require('node-bourbon'),
    sass = require('gulp-sass'),
    babel = require('gulp-babel'),
    watch = require('gulp-watch'),
    wait = require('gulp-wait'),
    // wait = require('gulp-wait'),
    uglify = require('gulp-uglifyjs'),
    browserSync = require('browser-sync').create();


// Scss stylesheets
gulp.task('stylesheets', function() {
    return gulp.src('stylesheets/**/*.scss')
        // .pipe(wait(150))
        .pipe(wait(150))
        .pipe(sass({
            outputStyle: 'compressed',
            //includePaths: bourbon.includePaths
        })).on('error', sass.logError)
        .pipe(autoprefixer({
            browsers: ['last 3 versions']
        }))
        .pipe(gulp.dest('css/'))
        .pipe(browserSync.stream());
});

gulp.task('babel', function () {
    gulp.src('./js/src/main.js')
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(uglify())
        .pipe(gulp.dest('./js/min'))
        // .pipe(gulp.dest('./js/dist'))
});

gulp.task('watch', function() {
    watch(['./stylesheets/**/*.scss'], function(event, cb) {
        gulp.start('stylesheets');
    });
    watch(['./js/**/*.js'], function(event, cb) {
        console.log('js watch')
        gulp.start('babel');

    });
    // watch(['./js/dist/**/*.js'], function(event, cb) {
    //     gulp.start('uglify');
    // });

    gulp.watch("./*.html").on('change', browserSync.reload);
    gulp.watch("./js/min/**/*.js").on('change', browserSync.reload);
});

gulp.task('serve', function() {
    browserSync.init({
        server: "./",
        open: true,
        port: 8081
    });

});

gulp.task('uglify', function() {
    gulp.src('./js/dist/**/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('./js/min'))
});

// Run
gulp.task('default', [
    'stylesheets',
    'babel',
    'serve',
    // 'uglify',
    'watch'
]);

gulp.task('wp', [
    'stylesheets',
    'babel',
    'watch'
]);